<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\OurProductCategory::class, function (Faker $faker) {
    return [
        'category_name' => $faker->name,
    ];
});
