<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Banner::class, function (Faker $faker) {
    return [
        'foto' => $faker->imageUrl($width = 200, $height = 200),
        'title' => $faker->name,
        'sub_title' => $faker->name,
        'button' => $faker->name,
        'url' => $faker->url,
    ];
});
