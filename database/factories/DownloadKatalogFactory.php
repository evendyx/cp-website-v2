<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\DownloadKatalog::class, function (Faker $faker) {
    return [
        'nama_lengkap' => $faker->name,
        'perusahaan' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'no_telp' => $faker->phoneNumber,
    ];
});
