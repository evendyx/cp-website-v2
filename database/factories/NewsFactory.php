<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\News::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'image' => $faker->imageUrl($width = 200, $height = 200),
        'description' => $faker->text(),
        'source' => $faker->url,
    ];
});
