<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Contact::class, function (Faker $faker) {
    return [
        'nama' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'nomor_telephon' => 565876897,
        'subject' => $faker->name,
        'pesan' => $faker->text(),
    ];
});
