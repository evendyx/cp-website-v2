<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\ContactProfile::class, function (Faker $faker) {
    return [
        'nama' => $faker->name,
        'no_telp' => $faker->randomDigit,
        'whatsapp' => $faker->randomDigit,
        'email' => $faker->unique()->safeEmail,
    ];
});
