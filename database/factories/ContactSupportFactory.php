<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\ContactSupport::class, function (Faker $faker) {
    return [
        'info' => $faker->text(),
        'alamat' => $faker->address,
    ];
});
