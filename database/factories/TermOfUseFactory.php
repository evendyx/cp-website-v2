<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\TermOfUse::class, function (Faker $faker) {
    return [
        'deskripsi' => $faker->text(),
    ];
});
