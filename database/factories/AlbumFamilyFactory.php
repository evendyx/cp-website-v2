<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\AlbumFamily::class, function (Faker $faker) {
    return [
        'image' => $faker->imageUrl($width = 200, $height = 200),
    ];
});
