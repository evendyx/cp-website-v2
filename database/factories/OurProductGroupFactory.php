<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\OurProductGroup::class, function (Faker $faker) {
    return [
        'group_name' => $faker->name,
    ];
});
