<?php

use App\AboutProduct;
use App\AlbumFamily;
use App\Banner;
use App\Benefit;
use App\Contact;
use App\ContactProfile;
use App\ContactSupport;
use App\DownloadKatalog;
use App\Faq;
use App\Gulma;
use App\News;
use App\OurPartner;
use App\OurProduct;
use App\OurProductCategory;
use App\OurProductFaq;
use App\OurProductGroup;
use App\OurProductGulma;
use App\Pengunjung;
use App\PrivacyPolicy;
use App\Product;
use App\ProductCategory;
use App\ProductGroup;
use App\ProdukFaq;
use App\TentangKami;
use App\TermOfUse;
use App\Testimonial;
use App\User;
use App\VisiMisi;
use Illuminate\Database\Seeder;

class DummyDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(AboutProduct::class, 5)->create();
        factory(AlbumFamily::class, 5)->create();
        factory(Banner::class, 5)->create();
        factory(Benefit::class, 5)->create();
        factory(Contact::class, 5)->create();
        factory(ContactProfile::class, 5)->create();
        factory(ContactSupport::class, 5)->create();
        factory(DownloadKatalog::class, 5)->create();
        factory(Faq::class, 5)->create();
        factory(Gulma::class, 5)->create();
        factory(News::class, 5)->create();
        factory(OurPartner::class, 5)->create();
        factory(OurProductCategory::class, 5)->create();
        factory(OurProduct::class, 5)->create();
        factory(OurProductFaq::class, 5)->create();
        factory(OurProductGroup::class, 5)->create();
        factory(OurProductGulma::class, 5)->create();
        factory(Pengunjung::class, 5)->create();
        factory(PrivacyPolicy::class, 5)->create();
        factory(ProductCategory::class, 5)->create();
        factory(Product::class, 5)->create();
        factory(ProductGroup::class, 5)->create();
        factory(ProdukFaq::class, 5)->create();
        factory(TentangKami::class, 5)->create();
        factory(TermOfUse::class, 5)->create();
        factory(Testimonial::class, 5)->create();
        factory(VisiMisi::class, 5)->create();
        factory(User::class, 5)->create();
    }
}
